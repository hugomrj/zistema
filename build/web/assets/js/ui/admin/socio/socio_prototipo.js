

function Socio(){
    
   this.tipo = "socio";   
   this.recurso = "socios";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
      
   this.titulosin = "Socio"
   this.tituloplu = "Socios"   
      
   this.campoid=  'socio';
   this.tablacampos =  ['socio', 'cedula', 'nombre_apellido', 'dependencia',  'ingreso_funcionario' ];
   this.etiquetas =  ['Socio', 'Cedula', 'Nombre y Apellido', 'Dependencia',  'Ingreso funcionario'  ];

   this.tablaformat =  ['N', 'N', 'C', 'C', 'D' ];   
      
      
   this.tbody_id = "socio-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "socio-acciones";   
   
   this.parent = null;
   
   
    this.combobox = 
            {
                "relacion_laboral":{
                    "value":"relacion_laboral",
                    "inner":"descripcion"} ,       
                "direccion_sueldo":{
                    "value":"direccion_sueldo",
                    "inner":"descripcion"}                                             
            };   




}







Socio.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};






Socio.prototype.form_validar = function() {    
    
     
    var socio_cedula = document.getElementById('socio_cedula');        
    if (parseInt(NumQP(socio_cedula.value)) <= 0 )         
    {
        msg.error.mostrar("Falta numero de cedula");    
        socio_cedula.focus();
        socio_cedula.select();                
        return false;
    }        
   

    
    var socio_nombre_apellido = document.getElementById('socio_nombre_apellido');    
    if (socio_nombre_apellido.value == "")         
    {
        msg.error.mostrar("Falta nombre y apellido" );           
        socio_nombre_apellido.focus();
        socio_nombre_apellido.select();        
        return false;
    }       
    
    
    
    return true;
};







Socio.prototype.form_ini = function() {    
    

    var socio_cedula = document.getElementById('socio_cedula');            
     socio_cedula.onblur  = function() {                  
         socio_cedula.value = fmtNum(socio_cedula.value);      
    };      
    socio_cedula.onblur();          
    


    
};








Socio.prototype.carga_combos = function( obj  ) {                
    
   
   
        var socio_relacion_laboral = document.getElementById("socio_relacion_laboral");
        var idedovalue = socio_relacion_laboral.value;
   
        ajax.url = html.url.absolute()+'/api/relacionlaboral/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();     

        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['relacion_laboral'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                socio_relacion_laboral.appendChild(opt);                     
            }
            
        }
            
            

            
            
        var socio_direccion_sueldo = document.getElementById("socio_direccion_sueldo");
        var idedovalue = socio_direccion_sueldo.value;
   
        ajax.url = html.url.absolute()+'/api/direccionsueldo/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();     

                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['direccion_sueldo'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                socio_direccion_sueldo.appendChild(opt);                     
            }
            
        }            
            
                     
            
            

};





