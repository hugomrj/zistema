

function CasaComercial(){
    
   this.tipo = "casacomercial";   
   this.recurso = "casascomerciales";   
   this.value = 0;
   this.form_descrip  = "casacomercial_descripcion";
   this.json_descrip = "nombre";
   
   
   this.dom="";
   this.carpeta=  "/admin";   
      
      
   this.titulosin = "Casa Comercial"
   this.tituloplu = "Casas Comerciales"   
      
      
   this.campoid=  'casacomercial';
   this.tablacampos =  ['casacomercial', 'nombre', 'direccion', 'telefono',  'email' ];
   this.etiquetas =  [ 'Codigo', 'Nombre', 'Direccion', 'Telefono',  'Email' ];

   this.tablaformat =  ['N', 'C', 'C', 'C', 'C' ];   
      
   this.tbody_id = "casacomercial-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "casacomercial-acciones";   
   
   this.parent = null;
   

}







CasaComercial.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};






CasaComercial.prototype.form_validar = function() {    
    



    var casacomercial_nombre = document.getElementById('casacomercial_nombre');    
    if (casacomercial_nombre.value == "")         
    {
        msg.error.mostrar("Falta nombre de Casa Comercial" );           
        casacomercial_nombre.focus();
        casacomercial_nombre.select();        
        return false;
    }       
    

    
    
    return true;
};







CasaComercial.prototype.form_ini = function() {    
    

    
};










