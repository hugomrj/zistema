

function ServicioFijoDescuento(){
    
   this.tipo = "serviciofijodescuento";   
   this.recurso = "serviciofijodescuentos";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   this.dom="";
   this.carpeta=  "/configuracion";   
      
   
   
   this.titulosin = "Servicio fijo descuento"
   this.tituloplu = "Servicios fijos descuentos"   
      
   this.campoid=  'id';
   this.tablacampos =  ['fecha', 'servicio.descripcion', 'mes',  'agno'  ];
   this.etiquetas =  ['Fecha', 'Servicio', 'Mes', 'Año'];
   
   this.tablaformat =  ['D', 'C', 'N', 'N' ];      
   
   this.tbody_id = "serviciofijodescuento-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "serviciofijodescuento-acciones";   
   
   this.parent = null;
   
   /*
    this.combobox = {"tipo_descuento":{
         "value":"tipo_descuento",
         "inner":"descripcion"}};      
   */
   
}







ServicioFijoDescuento.prototype.new = function( obj  ) {   
    

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
      
      
    document.getElementById( 'serviciofijodescuento_agno' ).value = new Date().getFullYear();    
    document.getElementById( 'serviciofijodescuento_sel_mes' ).value = (new Date().getMonth() +1);      
    
    
    // botones
    ServicioFijoDescuento_form_add(obj);
    


    
      
};





    

ServicioFijoDescuento.prototype.form_ini = function() {    
    
    var serviciofijodescuento_agno = document.getElementById('serviciofijodescuento_agno');          
    serviciofijodescuento_agno.onblur  = function() {                
        serviciofijodescuento_agno.value  = fmtNum(serviciofijodescuento_agno.value);
        serviciofijodescuento_agno.value  = NumQP(serviciofijodescuento_agno.value);
    };     
    serviciofijodescuento_agno.onblur();          
       
    
    
    
};
 



ServicioFijoDescuento.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);       

};





ServicioFijoDescuento.prototype.form_validar = function() {    
    
    
    
    var serviciofijodescuento_agno = document.getElementById('serviciofijodescuento_agno');     
    if (parseInt(NumQP(serviciofijodescuento_agno.value)) <= 0 )         
    {
        
        msg.error.mostrar("Falta Año");            
        serviciofijodescuento_agno.focus();
        serviciofijodescuento_agno.select();         
        
        return false;
    }        
    
    
    
    return true;
    
};






ServicioFijoDescuento.prototype.carga_combos = function( obj  ) {                
    
   
   
        var serviciofijodescuento_servicio = document.getElementById("serviciofijodescuento_servicio");
        var idedovalue = serviciofijodescuento_servicio.value;
   
        ajax.url = html.url.absolute()+'/api/conceptoservicio/1/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();         
    
                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['servicio'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                serviciofijodescuento_servicio.appendChild(opt);                     
            }
            
        }
            

};





ServicioFijoDescuento.prototype.post_form_id = function( obj  ) {                
    
        ServicioFijoDescuento_form_reg(  obj  );
   

};








