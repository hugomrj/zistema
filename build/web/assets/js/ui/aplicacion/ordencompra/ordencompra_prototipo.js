

function OrdenCompra(){
    
   this.tipo = "ordencompra";   
   this.recurso = "ordenescompras";   
   this.value = 0;
   this.from_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   
   this.dom="";
   this.carpeta=  "/aplicacion";   
      
      
   this.titulosin = "Orden Compra"
   this.tituloplu = "Ordenes Compras"   
      
   this.campoid=  'ordencompra';
   this.tablacampos =  ['ordencompra', 'fecha', 'socio.nombre_apellido',  'casacomercial.nombre' , 'credito' ,
            'cantidad_cuotas',  'cuotas_pagadas', 'credito_saldo' ];
   this.etiquetas =  ['Orden nro', 'Fecha', 'Socio', 'Casa comercial',  'Credito', 'Cuotas' ,  'Pagadas',  'Saldo'  ];

   this.tablaformat =  ['N', 'D', 'C', 'C', 'N', 'N', 'N', 'N'  ];   
      
      
   this.tbody_id = "ordencompra-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "ordencompra-acciones";   
   
   this.parent = null;
   
   

}




OrdenCompra.prototype.new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
    
    
        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
          dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
          mes='0'+mes //agrega cero si el menor de 10
        var f =   ano+"-"+mes+"-"+dia;      
    
    
    
    var ordencompra_fecha = document.getElementById('ordencompra_fecha');    
    ordencompra_fecha.value  = f;
    
    
    var ordencompra_monto_cuota = document.getElementById('ordencompra_monto_cuota');
    ordencompra_monto_cuota.disabled = true;
    
    
    var saldoorden = document.getElementById('saldoorden');
    saldoorden.style.display = "none";
    
    var line_user = document.getElementById('line_user');
    line_user.style.display = "none";
    
    
};






OrdenCompra.prototype.form_validar = function() {    

    
    
    
    var ordencompra_fecha = document.getElementById('ordencompra_fecha');
    if (ordencompra_fecha.value == ""){
        msg.error.mostrar("Error en fecha ");                    
        ordencompra_fecha.focus();
        ordencompra_fecha.select();                                       
        return false;        
    }
    
    
     
    var ordencompra_socio = document.getElementById('ordencompra_socio');        
    if (parseInt(NumQP(ordencompra_socio.value)) <= 0 )         
    {
        msg.error.mostrar("Falta seleccionar socio");    
        ordencompra_socio.focus();
        ordencompra_socio.select();                
        return false;
    }        
   
     
    var ordencompra_casacomercial = document.getElementById('ordencompra_casacomercial');        
    if (parseInt(NumQP(ordencompra_casacomercial.value)) <= 0 )         
    {
        msg.error.mostrar("Falta seleccionar casa comercial");    
        ordencompra_casacomercial.focus();
        ordencompra_casacomercial.select();                
        return false;
    }        
   
     
     
    var ordencompra_credito = document.getElementById('ordencompra_credito');        
    if (parseInt(NumQP(ordencompra_credito.value)) <= 0 )         
    {
        msg.error.mostrar("Falta monto del credito");    
        ordencompra_credito.focus();
        ordencompra_credito.select();                
        return false;
    }        
   
     
    var ordencompra_cantidad_cuotas = document.getElementById('ordencompra_cantidad_cuotas');        
    if (parseInt(NumQP(ordencompra_cantidad_cuotas.value)) <= 0 )         
    {
        msg.error.mostrar("Falta cantidad de cuotas");    
        ordencompra_cantidad_cuotas.focus();
        ordencompra_cantidad_cuotas.select();                
        return false;
    }        
   

    
    
    
    return true;
};







OrdenCompra.prototype.form_ini = function() {    
    

    var ordencompra_socio = document.getElementById('ordencompra_socio');            
     ordencompra_socio.onblur  = function() {                  
         ordencompra_socio.value = fmtNum(ordencompra_socio.value);      
         ordencompra_socio.value = NumQP(ordencompra_socio.value);      
    
        var  id = (ordencompra_socio.value );
        
        ajax.url = html.url.absolute()+'/api/socios/'+id;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      

        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;      
            
            document.getElementById('cliente_descripcion').innerHTML = 
                    oJson['nombre_apellido']; 
        }
        else{
            document.getElementById('cliente_descripcion').innerHTML = "";
        }   
    
     };      
    ordencompra_socio.onblur();          


    var ico_more_ordencompra_socio = document.getElementById('ico-more-ordencompra_socio');              
    ico_more_ordencompra_socio.onclick = function(  )
    {  
            var obj = new Socio();                 
            
            obj.acctionresul = function(id) {    
                ordencompra_socio.value = id; 
                ordencompra_socio.onblur(); 
                
            };               
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
    };       
    
    







    var ordencompra_casacomercial = document.getElementById('ordencompra_casacomercial');            
    ordencompra_casacomercial.onblur  = function() {                  
         ordencompra_casacomercial.value = fmtNum(ordencompra_casacomercial.value);      
         ordencompra_casacomercial.value = NumQP(ordencompra_casacomercial.value);      
 
            var  id = (ordencompra_casacomercial.value );

            ajax.url = html.url.absolute()+'/api/casascomerciales/'+id;
            ajax.metodo = "GET";   
            var datajson = ajax.private.json();      

            if (ajax.state == 200)
            {            
                var oJson = JSON.parse(datajson) ;      

                document.getElementById('casacomercial_descripcion').innerHTML = 
                        oJson['nombre']; 
                
                var ordencompra_credito = document.getElementById('ordencompra_credito');
                ordencompra_credito.focus();
                ordencompra_credito.select();                

                
            }
            else{
                document.getElementById('casacomercial_descripcion').innerHTML = "";
            }   
     };      
    ordencompra_casacomercial.onblur();          


    var ico_more_ordencompra_casacomercial = document.getElementById('ico-more-ordencompra_casacomercial');              
    ico_more_ordencompra_casacomercial.onclick = function(  )
    {  
            var obj = new CasaComercial();                 
            
            obj.acctionresul = function(id) {    
                ordencompra_casacomercial.value = id; 
                ordencompra_casacomercial.onblur(); 
                
            };               
            modal.ancho = 900;
            busqueda.modal.objeto(obj);
    };       
    
    


    var ordencompra_credito = document.getElementById('ordencompra_credito');            
     ordencompra_credito.onblur  = function() {                  
         ordencompra_credito.value = fmtNum(ordencompra_credito.value);      
         
         resultado_monto_cuota();
    };      
    ordencompra_credito.onblur();          
    

    var ordencompra_cantidad_cuotas = document.getElementById('ordencompra_cantidad_cuotas');            
     ordencompra_cantidad_cuotas.onblur  = function() {                  
         ordencompra_cantidad_cuotas.value = fmtNum(ordencompra_cantidad_cuotas.value);      
        resultado_monto_cuota();
    };      
    ordencompra_cantidad_cuotas.onblur();          
    
  
    var ordencompra_monto_cuota = document.getElementById('ordencompra_monto_cuota');            
     ordencompra_monto_cuota.onblur  = function() {                  
         ordencompra_monto_cuota.value = fmtNum(ordencompra_monto_cuota.value);      
    };      
    ordencompra_monto_cuota.onblur();          
    


  
    var ordencompra_credito_saldo = document.getElementById('ordencompra_credito_saldo');            
     ordencompra_credito_saldo.onblur  = function() {                  
         ordencompra_credito_saldo.value = fmtNum(ordencompra_credito_saldo.value);      
    };      
    ordencompra_credito_saldo.onblur();          
    
  
    var ordencompra_cuotas_pagadas = document.getElementById('ordencompra_cuotas_pagadas');            
     ordencompra_cuotas_pagadas.onblur  = function() {                  
         ordencompra_cuotas_pagadas.value = fmtNum(ordencompra_cuotas_pagadas.value);      
    };      
    ordencompra_cuotas_pagadas.onblur();          
    

    
    
    
 
};








OrdenCompra.prototype.form_edit = function( obj  ) {                



    var ordencompra_monto_cuota = document.getElementById('ordencompra_monto_cuota');            
    ordencompra_monto_cuota.disabled = true;

    var ordencompra_credito_saldo = document.getElementById('ordencompra_credito_saldo');            
    ordencompra_credito_saldo.disabled = true;
  
    var ordencompra_cuotas_pagadas = document.getElementById('ordencompra_cuotas_pagadas');            
    ordencompra_cuotas_pagadas.disabled = true;    
        

    document.getElementById('ordencompra-reportes').style.display = "none";            

    
    var ordencompra_user_add = document.getElementById('ordencompra_user_add');            
    ordencompra_user_add.disabled = true;    
    
    
    
};





OrdenCompra.prototype.form_del = function( obj  ) {                
 
    document.getElementById('ordencompra-reportes').style.display = "none";            
    
};








OrdenCompra.prototype.post_form_id = function( obj  ) {                

    
    //alert("post_form_id");
    

            boton.blabels = ['Imprimir'];
            var strhtml =   boton.get_botton_base() ;
            document.getElementById(  obj.tipo + '-reportes' ).innerHTML = strhtml;



                
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_imprimir');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {       

 
                        var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');            

                        ajax.url = html.url.absolute()+"/OrdenCompra/Reporte/ordencompra.pdf"
                            +"?ordencompra="+ordencompra_ordencompra.value

                        ajax.private.jasper();     

                                                
                            
                            
                           
                },
                false
            );   
  


    
    
};






OrdenCompra.prototype.get_api= function( obj  ) {                

 
        var restapi = "";
 
        var sel=  document.getElementById('archivoxls_sel_mes');
        
        //var mes=  sel.options[sel.selectedIndex].value;
        var mes=  obj.mes;

    
        var agno = document.getElementById('agno').value ; 
    
        restapi = "ordenescompras/mes/"+mes+"/agno/"+agno;
        return restapi;


};








OrdenCompra.prototype.pre_lista= function( obj  ) {                
  
  
        var selmes=document.getElementById('archivoxls_sel_mes');

        selmes.children[obj.mes -1].selected=true;
  
   
   
   
        var conagno =  NumQP (fmtNum(document.getElementById('agno').value) ); 
        
        if (conagno == '0'){
            document.getElementById('agno').value = new Date().getFullYear();  
        }
   
   
        
 
};




OrdenCompra.prototype.post_lista= function( obj  ) {                

        var selmes =  document.getElementById('archivoxls_sel_mes');
        selmes.onchange = function(){
    
            var mes = selmes.options[selmes.selectedIndex].value;                  
            obj.mes = mes;

            paginacion.pagina =1;
            reflex.cabdet_paginacion(obj, paginacion.pagina);    
                
            //    alert("hola");
            
        };
 
 
 
 
        var agno =  document.getElementById('agno');
        agno.onchange = function(){
    
            selmes.onchange();
                
            //alert("agno");
            
        };
 
 
 
 
 
 
 
 
        //botonees de lista
        boton.objeto = ""+obj.tipo;
        
        document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new_prin_file();
    
    
    
            var btn_ordencompra_nuevo = document.getElementById('btn_ordencompra_nuevo');
            btn_ordencompra_nuevo.addEventListener('click',
                function(event) {     
                    
                    obj.new(obj);
                },
                false
            );   
  
  
  
            var btn_ordencompra_imprimir = document.getElementById('btn_ordencompra_imprimir');
            btn_ordencompra_imprimir.addEventListener('click',
                function(event) {       

                        var aa = document.getElementById('agno') ; 

                        ajax.url = html.url.absolute()+"/OrdenCompra/Reporte/lista.pdf"
                            +"?mes="+obj.mes
                            +"&aa="+aa.value
                        ajax.private.jasper();    
                },
                false
            );   
  
  
  
  
            var btn_ordencompra_excel = document.getElementById('btn_ordencompra_excel');
            btn_ordencompra_excel.addEventListener('click',
                function(event) {       
                         
                           

                        var aa = document.getElementById('agno') ; 

                        ajax.url = html.url.absolute()+"/ordencompra/lista.xls"
                            +"?aa="+aa.value
                            +"&mm="+obj.mes;


                        ajax.req.open("POST", ajax.url, true);
                        ajax.req.responseType = 'blob';
                        ajax.req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                        ajax.req.onload = function (e) {
                            if (ajax.req.readyState === 4 && ajax.req.status === 200) {
                                var contenidoEnBlob = ajax.req.response;
                                var link = document.createElement('a');
                                link.href = (window.URL || window.webkitURL).createObjectURL(contenidoEnBlob);

                                link.download = "ordenescompra_"+aa.value+"_"+obj.mes+".xls";

                                var clicEvent = new MouseEvent('click', {
                                    'view': window,
                                    'bubbles': true,
                                    'cancelable': true
                                });
                                //Simulamos un clic del usuario
                                //no es necesario agregar el link al DOM.
                                link.dispatchEvent(clicEvent);
                                //link.click();

    //console.log(ajax.req.getResponseHeader("token"));                            
                                ajax.headers.getResponse();                    

                            }
                            else 
                            {
                                if (ajax.req.status === 401){
                                        ajax.state = ajax.req.status;                   
                                        html.url.redirect(ajax.state);
                                }
                                else{
                                    alert(" No es posible acceder al archivo");
                                }

                            }
                        };                    


                        ajax.headers.setRequest();  

                        ajax.req.send();

                         
                           
                },
                false
            );   
  
  
  
  
  
    
    
};






OrdenCompra.prototype.main_list = function( obj  ) {        
    
    //busqueda.botoncompuesta = true;
        
        document.getElementById( obj.dom ).innerHTML =  "<article id=\"arti_cab\"></article><article id=\"arti_det\"></article>";
        reflex.interfase.cabdet( obj );
        
};





