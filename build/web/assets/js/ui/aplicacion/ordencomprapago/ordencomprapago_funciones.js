       
   
function inicio( ){    
    html_cab(  );
}





function html_cab( ){    
    
    var arti_cab  =   document.getElementById('arti_form') ;
    
    loader.inicio();

    xhr =  new XMLHttpRequest();
    var url = html.url.absolute()+"/aplicacion/ordencomprapago/htmf/cab.html";           
    var metodo = "GET";        
    var type = "text/html;charset=UTF-8";
    xhr.open( metodo.toUpperCase(),   url,  true );      


    xhr.onreadystatechange = function () {
        if ( xhr.readyState == 4 ){            
            if (xhr.status == 200){
                loader.fin();                
    
                arti_cab.innerHTML = xhr.responseText;    
                    
                form_main_acciones();
                
                form.name = "form_ordencompra" ;
                form.disabled(false);   
                html_det();
                                
                var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                ordencompra_ordencompra.disabled =  false
    
            }            
        }
    };
    xhr.onerror = function (e) {  
        msg.error.mostrar("error ");
    };         
    xhr.setRequestHeader('Content-Type', type);   
    xhr.send( null );                                            
        
}




   
   
function html_det(){    
    
    var arti_det  =   document.getElementById('ordencomprapagos_lista') ;    
    loader.inicio();

    xhr =  new XMLHttpRequest();
    var url = html.url.absolute()+"/aplicacion/ordencomprapago/htmf/det.html";           
    var metodo = "GET";        
    var type = "text/html;charset=UTF-8";
    xhr.open( metodo.toUpperCase(),   url,  true );      

    xhr.onreadystatechange = function () {
        if ( xhr.readyState == 4 ){            
            if (xhr.status == 200){
                loader.fin();                
                arti_det.innerHTML = xhr.responseText;


                //global_ordencompra  = 6;
                if (global_ordencompra != 0) {
                    var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                    ordencompra_ordencompra.value = global_ordencompra;
                    cargar_datos();           
                }


            }            
        }
    };
    xhr.onerror = function (e) {  
        msg.error.mostrar("error ");
    };         
    xhr.setRequestHeader('Content-Type', type);   
    xhr.send( null );                                                    
}
   
   
   
   
function cargar_datos (){
    
        var data = 0;

        loader.inicio();
        xhr =  new XMLHttpRequest();
        var url = html.url.absolute()+"/api/ordenescompras/"+ordencompra_ordencompra.value;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:

                        form_cargar_datos ( xhr.responseText);                                
                        form_main_detalles_json ( ordencompra_ordencompra.value  );
                        
                        ordencomprapagopago_acciones_lista (  ordencompra_ordencompra.value  );
                        
                        break;

                    case 204:                            
                        msg.error.mostrar( "No encontrado" );              
                        form_cargar_limpiar(); 
                        form_main_detalles_tabla_cargar ( "[]" )
                        break;

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );    
       
    
}   
   
   
   
   
function form_main_acciones(){
    
    var ico_more_ordencompra_buscar  =   document.getElementById('ico-more-ordencompra_buscar') ;    
    ico_more_ordencompra_buscar.addEventListener('click',
        function(event) {               
            form_busqueda_ordencompra();
        },
        false
    );        
    
    
    
    
    var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');            
    ordencompra_ordencompra.onblur  = function() {                  
        
        ordencompra_ordencompra.value = fmtNum(ordencompra_ordencompra.value);      
        ordencompra_ordencompra.value = NumQP(ordencompra_ordencompra.value);                      
        
        //if (ordencompra_ordencompra.value != 0 ) {
            cargar_datos ();        
        //};             
    }
         
         
    
    
    ordencompra_ordencompra.onkeypress  = function(event) {                               
        if ( event.keyCode == 13 ){
                ordencompra_ordencompra.blur();
        }
    }

    
} ;  








function form_busqueda_ordencompra(){    
    
    var arti_form  =  document.getElementById('arti_form') ;
    
    //loader.inicio();

    xhr =  new XMLHttpRequest();
    var url = html.url.absolute()+"/aplicacion/ordencompra/htmf/busqueda.html";           
    var metodo = "GET";        
    var type = "text/html;charset=UTF-8";
    xhr.open( metodo.toUpperCase(),   url,  true );      

    xhr.onreadystatechange = function () {
        if ( xhr.readyState == 4 ){            
            if (xhr.status == 200){
         
         //
                //loader.fin();                
                //arti_form.innerHTML = xhr.responseText;                
                var html = xhr.responseText;                                
          //      
                modal.ancho = 900;
                var venactiva = modal.ventana.html( "vid" , html) ;                   
                
                var dom = venactiva.firstChild.id;       
                modal.redim_ancho(document.getElementById(dom));            
                
                form_busqueda_ordencompra_accion();
                
            }            
        }
    };
    xhr.onerror = function (e) {  
        msg.error.mostrar("error ");
    };         
    xhr.setRequestHeader('Content-Type', type);   
    xhr.send( null );                                            
        
}






    function form_busqueda_ordencompra_accion(){

        var busqueda =   document.getElementById('busqueda') ;
        busqueda.addEventListener( 'keypress',
            function(event) {               

                var strlen = 0;

                if ( event.keyCode != 13 ){
                    strlen = (busqueda.value.length + 1);                               
                }
                else{
                    
                    cargar_datos_ordenesdecompra_busqueda ( busqueda.value ) ; 
                
                }        
            },
            false
        ); 
    
    
        var btn_busqueda_atras =   document.getElementById('btn_busqueda_atras') ;
        btn_busqueda_atras.addEventListener( 'click',
            function(event) {   
                //inicio();
                modal.ventana_cerrar("vid");
            },
            false
        );   
        
    
    
};




function form_cargar_datos ( json ){

        form.name = "form_ordencompra";
        form.json = json;           
        form.llenar();         
        
        
        //formatos
        var ordencompra_credito = document.getElementById('ordencompra_credito') ;
        ordencompra_credito.value =   fmtNum(ordencompra_credito.value); 

        
        var ordencompra_monto_cuota = document.getElementById('ordencompra_monto_cuota') ;
        ordencompra_monto_cuota.value =   fmtNum(ordencompra_monto_cuota.value); 

        
        var ordencompra_credito_saldo = document.getElementById('ordencompra_credito_saldo') ;
        ordencompra_credito_saldo.value =   fmtNum(ordencompra_credito_saldo.value); 

        
        
        
};




function form_cargar_limpiar (  ){

    form.json = JSON.stringify(cerojson);           
    form.llenar();         
 //   var busqueda =   document.getElementById('busqueda') ;
    var ordencomprapagopago_acciones = document.getElementById('ordencomprapagopago_acciones') ;    
    ordencomprapagopago_acciones.innerHTML =  "";
    

};










function form_main_detalles_json ( ordencompra  ){


        var data = 0;

        loader.inicio();
        xhr =  new XMLHttpRequest();
        var url = html.url.absolute()+"/api/ordenescompraspagos/ordencompra/"+ordencompra;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:
                        
                         form_main_detalles_tabla_cargar ( xhr.responseText );
                            
                        break;

                    case 204:                            
                        msg.error.mostrar( "No encontrado" );              
                        //form_cargar_limpiar(); 
                        break;

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );                                                     
  
};




function form_main_detalles_tabla_cargar ( json ){
    
        var obj = new OrdenCompraPago();  
        
        tabla.json = json;    
        
        tabla.ini(obj);
        tabla.gene();   
        tabla.formato(obj);
        
        //tabla.set.tablaid(obj);     
        //tabla.lista_registro(obj, reflex.form_id ); 
    
}









function ordencomprapagopago_acciones_lista ( ordencompra){
    
    var ordencomprapagopago_acciones = document.getElementById('ordencomprapagopago_acciones') ;
    
    boton.objeto = "ordencomprapagopago";
    ordencomprapagopago_acciones.innerHTML =  boton.basicform.get_botton_att();
    
    
    var btn_ordencomprapagopago_agregar = document.getElementById('btn_ordencomprapagopago_agregar') ;
    btn_ordencomprapagopago_agregar.addEventListener('click',
        function(event) {               
            
            var ordencompra_credito_saldo = document.getElementById('ordencompra_credito_saldo') ;
        
        
            if (ordencompra_credito_saldo.value <= 0 ){            
                msg.error.mostrar( "la orden de compra fue completamente pagada" );                  
            }
            else
            {  
                ordencomprapagopago_form( ordencompra );
            }
        },
        false
    );      
    
};




function ordencomprapagopago_form ( ordencompra ){
    
    var arti_form  =  document.getElementById('arti_form') ;    
    //loader.inicio();

    xhr =  new XMLHttpRequest();
    var url = html.url.absolute()+"/aplicacion/ordencomprapago/htmf/form.html";           
    var metodo = "GET";        
    var type = "text/html;charset=UTF-8";
    xhr.open( metodo.toUpperCase(),   url,  true );      

    xhr.onreadystatechange = function () {
        if ( xhr.readyState == 4 ){            
            if (xhr.status == 200){
         
       //         loader.fin();                
                //arti_form.innerHTML = xhr.responseText;                
                //ordencomprapagopago_form_agregar( ordencompra );
                //form_busqueda_ordencompra_accion();
             

                var html = xhr.responseText;                                   
                modal.ancho = 750;
                var venactiva = modal.ventana.html( "vid" , html) ;                   
                
                var dom = venactiva.firstChild.id;       
                modal.redim_ancho(document.getElementById(dom));            
               
                
                ordencomprapagopago_form_agregar( ordencompra );
                //form_busqueda_ordencompra_accion();
                
                
                
                
            }            
        }
    };
    xhr.onerror = function (e) {  
        msg.error.mostrar("error ");
    };         
    xhr.setRequestHeader('Content-Type', type);   
    xhr.send( null );                                  

    
};



function ordencomprapagopago_form_agregar( ordencompra ){
    
      
        var fecha = new Date(); //Fecha actual
        var mes = fecha.getMonth()+1; //obteniendo mes
        var dia = fecha.getDate(); //obteniendo dia
        var ano = fecha.getFullYear(); //obteniendo año
        if(dia<10)
          dia='0'+dia; //agrega cero si el menor de 10
        if(mes<10)
          mes='0'+mes //agrega cero si el menor de 10
        var f =   ano+"-"+mes+"-"+dia;
        
        var ordencomprapago_fecha = document.getElementById('ordencomprapago_fecha') ;  
        ordencomprapago_fecha.value = f;
  

        //ordencompra_credito_saldo    
        var ordencomprapago_credito_saldo = document.getElementById('ordencomprapago_credito_saldo') ;
        ordencomprapago_credito_saldo.value =
                document.getElementById('ordencompra_credito_saldo').value;
        ordencomprapago_credito_saldo.disabled = true;
        ordencomprapago_credito_saldo.value =   fmtNum(ordencomprapago_credito_saldo.value); 

        //ordencompra_monto_cuota
        var ordencomprapago_monto_cuota = document.getElementById('ordencomprapago_monto_cuota') ;
        ordencomprapago_monto_cuota.value =
                document.getElementById('ordencompra_monto_cuota').value;
        ordencomprapago_monto_cuota.value =   fmtNum(ordencomprapago_monto_cuota.value); 



        var ordencomprapago_ordencompra = document.getElementById('ordencomprapago_ordencompra') ;
        ordencomprapago_ordencompra.value = ordencompra ;


        var ordencomprapago_mes = document.getElementById('ordencomprapago_mes') ;
        //ordencomprapago_mes.value = 0;        
        ordencomprapago_mes.onblur  = function() {                  
             ordencomprapago_mes.value = fmtNum(ordencomprapago_mes.value);      
        };      
        ordencomprapago_mes.onblur();         
        


        var ordencomprapago_agno = document.getElementById('ordencomprapago_agno') ;
        ordencomprapago_agno.value = ano;



        var ordencomprapago_acciones = document.getElementById('ordencomprapago-acciones') ;

        boton.objeto = "ordencomprapago";    
        ordencomprapago_acciones .innerHTML =  boton.basicform.get_botton_add();



        var btn_ordencomprapago_guardar = document.getElementById('btn_ordencomprapago_guardar') ;
        btn_ordencomprapago_guardar.addEventListener ('click',
            function(event) {            

               //@POST
               //@Path("/ordencompra/{ordencompra}"  )
            
                var ordencomprapago = new OrdenCompraPago();
                
                if ( ordencomprapago.form_validar())
                {
                    
                    form.name  = "form_ordencomprapago";
                    var jsondata = form.datos.getjson();   
                    orden_compra_pago_agregar ( ordencompra, jsondata );
                    
                }               

            },
            false
        );      

    
    
    var btn_ordencomprapago_cancelar = document.getElementById('btn_ordencomprapago_cancelar') ;
    btn_ordencomprapago_cancelar.addEventListener('click',
        function(event) {            
           modal.ventana_cerrar("vid");
        },
        false
    );      
    
    
};





   
   
function cargar_datos_ordenesdecompra_busqueda ( buscar){
    
        var data = 0;
        
        loader.inicio();
        xhr =  new XMLHttpRequest();
        var url = html.url.absolute()+"/api/consulta/ordenescompras/nombreci;q="+buscar;           
        var metodo = "GET";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:

                        cargar_datos_ordenesdecompra_busqueda_tabla ( xhr.responseText );                       
                        //ordencomprapagopago_acciones_lista (  ordencompra_ordencompra.value  );

                        break;

                    case 204:                            
                        msg.error.mostrar( "No encontrado" );              
                        //form_cargar_limpiar(); 
                        //form_main_detalles_tabla_cargar ( "[]" )
                        break;

                    default:
                        msg.error.mostrar( xhr.status );
                        console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        xhr.setRequestHeader('Content-Type', type);   

        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( data );    

}   
   
   
   






function cargar_datos_ordenesdecompra_busqueda_tabla ( json ){
    
        var obj = new OrdenCompra();  
        
        tabla.json = json;    
        
        tabla.ini(obj);
        tabla.gene();   
        tabla.formato(obj);
        
        tabla.set.tablaid(obj);     
        tabla.lista_registro(obj, 
            function ( obj, id ){
                
                var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                ordencompra_ordencompra.value =   id
                
                ordencompra_ordencompra.value = fmtNum(ordencompra_ordencompra.value);      
                ordencompra_ordencompra.value = NumQP(ordencompra_ordencompra.value);                      
                cargar_datos ();        
                
                modal.ventana_cerrar("vid");
            
            }
    
        ); 

        
        
        
}







function orden_compra_pago_agregar ( ordencompra, jsondata  ){


        //var data = 0;

        loader.inicio();
        xhr =  new XMLHttpRequest();
        var url = html.url.absolute()+"/api/ordenescompraspagos/ordencompra/"+ordencompra;           
        var metodo = "POST";        
        var type = "application/json";
        xhr.open( metodo.toUpperCase(),   url,  true );      

        xhr.onreadystatechange = function () {
            if ( xhr.readyState == 4 ){
                loader.fin();

                ajax.local.token = xhr.getResponseHeader("token") ;            
                localStorage.setItem('token', xhr.getResponseHeader("token"));     
                //sessionStorage.setItem('total_registros',  xhr.getResponseHeader("total_registros"));

                switch ( xhr.status ) {
                    case 200:
                        
                        var ordencompra_ordencompra = document.getElementById('ordencompra_ordencompra');
                        ordencompra_ordencompra.value = fmtNum(ordencompra_ordencompra.value);      
                        ordencompra_ordencompra.value = NumQP(ordencompra_ordencompra.value);                      
                        cargar_datos ();        

                        modal.ventana_cerrar("vid");
                        msg.ok.mostrar( "cuota pagada" );  
                        
                        break;

                    case 204:                            
                        msg.error.mostrar( "error 204" );              
                        //form_cargar_limpiar(); 
                        break;

                    default:
                        msg.error.mostrar( xhr.responseText );
                        //console.log(xhr.responseText );
                }       
            }
        };
        xhr.onerror = function (e) {  
            msg.error.mostrar("error ");
        };         
        
        xhr.setRequestHeader('Content-Type', type);   
        xhr.setRequestHeader("token", localStorage.getItem('token'));
        xhr.send( jsondata );                                                     
  
};


