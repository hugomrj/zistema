select det.*, cab.*  from 
( 
select 1 as ord,   socio, ordenes_compras_pagos.mes,  ordenes_compras_pagos.agno, 
ordenes_compras.ordencompra numero, ordenes_compras.fecha, 'Orden de compra' as concepto,  
casas_comerciales.nombre as casa_comercial, 
concat( cast(numero_cuota as varchar), ' / ' ,cast(cantidad_cuotas as varchar) )  as cuotas, 
credito, ordenes_compras.monto_cuota as cuota, ordenes_compras_pagos.credito_saldo as saldo  
FROM aplicacion.ordenes_compras inner join aplicacion.casas_comerciales on  
(ordenes_compras.casacomercial = casas_comerciales.casacomercial ) 
inner join aplicacion.ordenes_compras_pagos on (ordenes_compras.ordencompra = ordenes_compras_pagos.ordencompra ) 
where  socio = v0  
and  ordenes_compras_pagos.agno  = v1         
and  ordenes_compras_pagos.mes  = v2        
union 

select 2 as ord,  socio, EXTRACT(MONTH FROM (fecha - interval '1 month'))  mes, 
EXTRACT(year FROM (fecha - interval '1 month')) agno, ordenes_compras.ordencompra numero, 
fecha, 'Orden de compra' as concepto, casas_comerciales.nombre as casa_comercial, 
concat( '0 / ' ,cast(cantidad_cuotas as varchar) )  as cuotas, 
credito, 0 as cuota, credito as saldo 
FROM aplicacion.ordenes_compras inner join aplicacion.casas_comerciales on  
(ordenes_compras.casacomercial = casas_comerciales.casacomercial ) 
where cuotas_pagadas  = 0 
and socio = v0  

union 
select 3 as ord,    socio,  ordenes_compras_pagos_seguro.mes, ordenes_compras_pagos_seguro.agno,  
ordenes_compras_pagos_seguro.id numero,    (ordenes_compras_pagos_seguro.fecha) fecha,  
servicio.descripcion as concepto, 'Atopama' as casa_comercial,  '1 / 1' as cuotas,    
    (ordenes_compras_pagos_seguro.monto_seguro) credito,     (ordenes_compras_pagos_seguro.monto_seguro) cuota,  
    0 credito_saldo 
FROM  
  aplicacion.ordenes_compras_pagos_seguro,  
  (  
        SELECT descripcion  FROM aplicacion.concepto_servicios where servicio = 3  
  ) as servicio  
WHERE  
        socio =  v0  
        and ordenes_compras_pagos_seguro.agno = v1         
        and ordenes_compras_pagos_seguro.mes = v2  
union          
select 4 as ord,   socios_descuentos.socio, socios_descuentos.mes,  socios_descuentos.agno, 
socios_descuentos.id numero, fecha,  concepto_servicios.descripcion as concepto, 'Atopama' as casa_comercial,  
 '1 / 1' as cuotas,  
monto_descuento, monto_descuento as cuota, 0 as saldo  
FROM aplicacion.socios_descuentos inner join  aplicacion.concepto_servicios  
on (socios_descuentos.servicio = concepto_servicios.servicio )  
where socios_descuentos.servicio =  2  
and socios_descuentos.socio = v0      
and  socios_descuentos.agno = v1      
and  socios_descuentos.mes  = v2    

union 

select 5 as ord, socio,  socios_descuentos_servicios.mes, socios_descuentos_servicios.agno, 
socios_descuentos_servicios.id numero, socios_descuentos_servicios.fecha, 
concepto_servicios.descripcion as concepto, 'Atopama' as casa_comercial, 
concat( cast(servicios_temporal_descuentos.cuota as varchar), ' / ' ,cast(servicios_temporal_descuentos.cuotas  as varchar) )  as cuotas,  
(monto_descuento * servicios_temporal_descuentos.cuotas) monto_descuento, 
monto_descuento as cuota, 
(monto_descuento * (servicios_temporal_descuentos.cuotas - servicios_temporal_descuentos.cuota)) saldo 
FROM aplicacion.socios_descuentos_servicios inner join  aplicacion.concepto_servicios   
on (socios_descuentos_servicios.servicio = concepto_servicios.servicio ) 
inner join aplicacion.servicios_temporal_descuentos on  
(socios_descuentos_servicios.id_cab  = servicios_temporal_descuentos.id ) 
where socios_descuentos_servicios.socio = v0 
and socios_descuentos_servicios.agno = v1 
and socios_descuentos_servicios.mes = v2 

) as det, 
( 
SELECT socio, cedula, nombre_apellido, telefono, celular, direccion_particular, direccion_laboral, dependencia,  
socios.relacion_laboral, ingreso_funcionario, estado, relacion_laboral.descripcion  
FROM aplicacion.socios inner join aplicacion.relacion_laboral 
on (socios.relacion_laboral = relacion_laboral.relacion_laboral ) ) as cab 
where cab.socio = det.socio  
order by ord, numero, fecha desc  
