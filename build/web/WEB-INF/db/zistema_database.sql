--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.24
-- Dumped by pg_dump version 9.5.24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: apertura_diaria; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.apertura_diaria (
    apertura integer NOT NULL,
    fecha date
);


ALTER TABLE aplicacion.apertura_diaria OWNER TO postgres;

--
-- Name: apertura_diaria_apertura_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.apertura_diaria_apertura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.apertura_diaria_apertura_seq OWNER TO postgres;

--
-- Name: apertura_diaria_apertura_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.apertura_diaria_apertura_seq OWNED BY aplicacion.apertura_diaria.apertura;


--
-- Name: apertura_diaria_detalle; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.apertura_diaria_detalle (
    id integer NOT NULL,
    producto integer,
    precio_venta bigint,
    apertura_cantidad integer,
    cierre_cantidad integer,
    venta_cantidad integer,
    monto_ingreso bigint,
    apertura integer
);


ALTER TABLE aplicacion.apertura_diaria_detalle OWNER TO postgres;

--
-- Name: apertura_diaria_detalle_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.apertura_diaria_detalle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.apertura_diaria_detalle_id_seq OWNER TO postgres;

--
-- Name: apertura_diaria_detalle_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.apertura_diaria_detalle_id_seq OWNED BY aplicacion.apertura_diaria_detalle.id;


--
-- Name: clientes; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.clientes (
    cliente integer NOT NULL,
    nombre character varying,
    telefono character varying,
    iden_doc character(1),
    iden_numero character varying NOT NULL,
    iden_digito character(1) DEFAULT 0,
    direccion character varying
);


ALTER TABLE aplicacion.clientes OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.clientes_cliente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.clientes_cliente_seq OWNER TO postgres;

--
-- Name: clientes_cliente_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.clientes_cliente_seq OWNED BY aplicacion.clientes.cliente;


--
-- Name: factura_ventas; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.factura_ventas (
    factura integer NOT NULL,
    factura_numero character varying,
    fecha_factura date DEFAULT now() NOT NULL,
    cliente integer,
    gravada0 bigint DEFAULT 0,
    gravada5 bigint DEFAULT 0,
    gravada10 bigint DEFAULT 0,
    iva5 bigint DEFAULT 0,
    iva10 bigint DEFAULT 0,
    monto_total bigint DEFAULT 0,
    usuario integer,
    forma_pago character varying(2),
    total_iva bigint DEFAULT 0,
    estado character(1),
    emisor integer,
    importe_pagado bigint DEFAULT 0,
    importe_vuelto bigint DEFAULT 0
);


ALTER TABLE aplicacion.factura_ventas OWNER TO postgres;

--
-- Name: factura_ventas_detalle; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.factura_ventas_detalle (
    venta_detalle integer NOT NULL,
    factura bigint DEFAULT 0,
    descripcion character varying(200),
    cantidad numeric(10,2) DEFAULT 0,
    precio bigint DEFAULT 0,
    sub_total bigint DEFAULT 0,
    gravada0 bigint DEFAULT 0,
    gravada5 bigint DEFAULT 0,
    gravada10 bigint DEFAULT 0,
    producto integer DEFAULT 0,
    precio_categoria integer NOT NULL
);


ALTER TABLE aplicacion.factura_ventas_detalle OWNER TO postgres;

--
-- Name: factura_ventas_detalle_venta_detalle_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.factura_ventas_detalle_venta_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.factura_ventas_detalle_venta_detalle_seq OWNER TO postgres;

--
-- Name: factura_ventas_detalle_venta_detalle_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.factura_ventas_detalle_venta_detalle_seq OWNED BY aplicacion.factura_ventas_detalle.venta_detalle;


--
-- Name: factura_ventas_factura_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.factura_ventas_factura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.factura_ventas_factura_seq OWNER TO postgres;

--
-- Name: factura_ventas_factura_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.factura_ventas_factura_seq OWNED BY aplicacion.factura_ventas.factura;


--
-- Name: formas_pagos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.formas_pagos (
    forma_pago character(2) NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.formas_pagos OWNER TO postgres;

--
-- Name: identificador_documentos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.identificador_documentos (
    iden_doc character(1) NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.identificador_documentos OWNER TO postgres;

--
-- Name: lista_precios_productos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.lista_precios_productos (
    lista_precio integer NOT NULL,
    producto integer NOT NULL,
    precio_categoria integer NOT NULL,
    monto bigint
);


ALTER TABLE aplicacion.lista_precios_productos OWNER TO postgres;

--
-- Name: porcentajes_iva; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.porcentajes_iva (
    porcentaje_iva integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.porcentajes_iva OWNER TO postgres;

--
-- Name: precios_categorias; Type: TABLE; Schema: aplicacion; Owner: hugo
--

CREATE TABLE aplicacion.precios_categorias (
    precio_categoria integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.precios_categorias OWNER TO hugo;

--
-- Name: precios_categorias_preciocate_seq; Type: SEQUENCE; Schema: aplicacion; Owner: hugo
--

CREATE SEQUENCE aplicacion.precios_categorias_preciocate_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.precios_categorias_preciocate_seq OWNER TO hugo;

--
-- Name: precios_categorias_preciocate_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: hugo
--

ALTER SEQUENCE aplicacion.precios_categorias_preciocate_seq OWNED BY aplicacion.precios_categorias.precio_categoria;


--
-- Name: productos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.productos (
    producto integer NOT NULL,
    categoria integer,
    nombre character varying,
    codigo character varying,
    codigo_barras character varying,
    porcentaje_iva integer
);


ALTER TABLE aplicacion.productos OWNER TO postgres;

--
-- Name: productos_categorias; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE aplicacion.productos_categorias (
    categoria integer NOT NULL,
    descripcion character varying
);


ALTER TABLE aplicacion.productos_categorias OWNER TO postgres;

--
-- Name: productos_categorias_categoria_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.productos_categorias_categoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.productos_categorias_categoria_seq OWNER TO postgres;

--
-- Name: productos_categorias_categoria_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.productos_categorias_categoria_seq OWNED BY aplicacion.productos_categorias.categoria;


--
-- Name: productos_lista_precios_lista_precio_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.productos_lista_precios_lista_precio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.productos_lista_precios_lista_precio_seq OWNER TO postgres;

--
-- Name: productos_lista_precios_lista_precio_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.productos_lista_precios_lista_precio_seq OWNED BY aplicacion.lista_precios_productos.lista_precio;


--
-- Name: productos_producto_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE aplicacion.productos_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE aplicacion.productos_producto_seq OWNER TO postgres;

--
-- Name: productos_producto_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE aplicacion.productos_producto_seq OWNED BY aplicacion.productos.producto;


--
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles (
    rol integer NOT NULL,
    nombre_rol character varying(140)
);


ALTER TABLE sistema.roles OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_rol_seq OWNER TO postgres;

--
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_rol_seq OWNED BY sistema.roles.rol;


--
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE sistema.roles_x_selectores OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.roles_x_selectores_id_seq OWNER TO postgres;

--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.roles_x_selectores_id_seq OWNED BY sistema.roles_x_selectores.id;


--
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE sistema.selectores OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_id_seq OWNER TO postgres;

--
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_id_seq OWNED BY sistema.selectores.id;


--
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE sistema.selectores_x_webservice OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.selectores_x_webservice_id_seq OWNER TO postgres;

--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.selectores_x_webservice_id_seq OWNED BY sistema.selectores_x_webservice.id;


--
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE sistema.usuarios OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_usuario_seq OWNER TO postgres;

--
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_usuario_seq OWNED BY sistema.usuarios.usuario;


--
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE sistema.usuarios_x_roles OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.usuarios_x_roles_id_seq OWNER TO postgres;

--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.usuarios_x_roles_id_seq OWNED BY sistema.usuarios_x_roles.id;


--
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE sistema.webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE sistema.webservice OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE sistema.webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sistema.webservice_wservice_seq OWNER TO postgres;

--
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE sistema.webservice_wservice_seq OWNED BY sistema.webservice.wservice;


--
-- Name: apertura; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria ALTER COLUMN apertura SET DEFAULT nextval('aplicacion.apertura_diaria_apertura_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria_detalle ALTER COLUMN id SET DEFAULT nextval('aplicacion.apertura_diaria_detalle_id_seq'::regclass);


--
-- Name: cliente; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes ALTER COLUMN cliente SET DEFAULT nextval('aplicacion.clientes_cliente_seq'::regclass);


--
-- Name: factura; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.factura_ventas ALTER COLUMN factura SET DEFAULT nextval('aplicacion.factura_ventas_factura_seq'::regclass);


--
-- Name: venta_detalle; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.factura_ventas_detalle ALTER COLUMN venta_detalle SET DEFAULT nextval('aplicacion.factura_ventas_detalle_venta_detalle_seq'::regclass);


--
-- Name: lista_precio; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.lista_precios_productos ALTER COLUMN lista_precio SET DEFAULT nextval('aplicacion.productos_lista_precios_lista_precio_seq'::regclass);


--
-- Name: precio_categoria; Type: DEFAULT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.precios_categorias ALTER COLUMN precio_categoria SET DEFAULT nextval('aplicacion.precios_categorias_preciocate_seq'::regclass);


--
-- Name: producto; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.productos ALTER COLUMN producto SET DEFAULT nextval('aplicacion.productos_producto_seq'::regclass);


--
-- Name: categoria; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.productos_categorias ALTER COLUMN categoria SET DEFAULT nextval('aplicacion.productos_categorias_categoria_seq'::regclass);


--
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles ALTER COLUMN rol SET DEFAULT nextval('sistema.roles_rol_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('sistema.roles_x_selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('sistema.selectores_x_webservice_id_seq'::regclass);


--
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios ALTER COLUMN usuario SET DEFAULT nextval('sistema.usuarios_usuario_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('sistema.usuarios_x_roles_id_seq'::regclass);


--
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice ALTER COLUMN wservice SET DEFAULT nextval('sistema.webservice_wservice_seq'::regclass);


--
-- Data for Name: apertura_diaria; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.apertura_diaria (apertura, fecha) FROM stdin;
1	2020-11-01
2	2020-11-02
3	2020-11-28
\.


--
-- Name: apertura_diaria_apertura_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.apertura_diaria_apertura_seq', 3, true);


--
-- Data for Name: apertura_diaria_detalle; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.apertura_diaria_detalle (id, producto, precio_venta, apertura_cantidad, cierre_cantidad, venta_cantidad, monto_ingreso, apertura) FROM stdin;
2	3	5000	0	\N	\N	\N	1
3	4	3500	0	\N	\N	\N	1
4	1	15000	0	\N	\N	\N	1
1	2	15000	10	5	5	75000	1
5	2	15000	0	\N	\N	\N	2
6	3	5000	0	\N	\N	\N	2
7	4	3500	0	\N	\N	\N	2
8	1	15000	0	\N	\N	\N	2
9	2	15000	0	\N	\N	\N	3
10	3	5000	0	\N	\N	\N	3
11	4	3500	0	\N	\N	\N	3
12	1	15000	0	\N	\N	\N	3
\.


--
-- Name: apertura_diaria_detalle_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.apertura_diaria_detalle_id_seq', 12, true);


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.clientes (cliente, nombre, telefono, iden_doc, iden_numero, iden_digito, direccion) FROM stdin;
1	Cliente de prueba	45698	R	123	4	direccion
0	Sin nombre	\N	C	0	0	\N
\.


--
-- Name: clientes_cliente_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.clientes_cliente_seq', 1, true);


--
-- Data for Name: factura_ventas; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.factura_ventas (factura, factura_numero, fecha_factura, cliente, gravada0, gravada5, gravada10, iva5, iva10, monto_total, usuario, forma_pago, total_iva, estado, emisor, importe_pagado, importe_vuelto) FROM stdin;
3		2021-02-05	1	0	0	2000	0	182	2000	1	CO	182	\N	\N	5000	3000
4		2021-02-11	0	40000	25000	0	1190	0	65000	1	CO	1190	\N	\N	70000	5000
\.


--
-- Data for Name: factura_ventas_detalle; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.factura_ventas_detalle (venta_detalle, factura, descripcion, cantidad, precio, sub_total, gravada0, gravada5, gravada10, producto, precio_categoria) FROM stdin;
3	3	Botella Corona	2.00	1000	2000	0	0	2000	1	1
4	4	Heineken 3/4	4.00	10000	40000	40000	0	0	2	1
5	4	Leche Trebol 1 litro	5.00	5000	25000	0	25000	0	6	1
\.


--
-- Name: factura_ventas_detalle_venta_detalle_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.factura_ventas_detalle_venta_detalle_seq', 5, true);


--
-- Name: factura_ventas_factura_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.factura_ventas_factura_seq', 4, true);


--
-- Data for Name: formas_pagos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.formas_pagos (forma_pago, descripcion) FROM stdin;
CO	CONTADO
CR	CREDITO
\.


--
-- Data for Name: identificador_documentos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.identificador_documentos (iden_doc, descripcion) FROM stdin;
R	Ruc
C	Cedula
\.


--
-- Data for Name: lista_precios_productos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.lista_precios_productos (lista_precio, producto, precio_categoria, monto) FROM stdin;
18	1	2	500
21	1	1	1000
22	2	2	9000
23	2	1	10000
24	6	1	5000
\.


--
-- Data for Name: porcentajes_iva; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.porcentajes_iva (porcentaje_iva, descripcion) FROM stdin;
10	10%
5	5%
0	exenta
\.


--
-- Data for Name: precios_categorias; Type: TABLE DATA; Schema: aplicacion; Owner: hugo
--

COPY aplicacion.precios_categorias (precio_categoria, descripcion) FROM stdin;
2	Mayorista
1	Minorista
\.


--
-- Name: precios_categorias_preciocate_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: hugo
--

SELECT pg_catalog.setval('aplicacion.precios_categorias_preciocate_seq', 2, true);


--
-- Data for Name: productos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.productos (producto, categoria, nombre, codigo, codigo_barras, porcentaje_iva) FROM stdin;
2	1	Heineken 3/4	CER		0
4	1	Pilsen.i			5
1	1	Botella Corona			10
3	1	Amstel tubito			10
6	6	Leche Trebol 1 litro			5
\.


--
-- Data for Name: productos_categorias; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY aplicacion.productos_categorias (categoria, descripcion) FROM stdin;
2	Powerade
3	Jugos 
4	Galletitas
5	Cigarrillos 
1	Cervezas
6	Lacteos
\.


--
-- Name: productos_categorias_categoria_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.productos_categorias_categoria_seq', 6, true);


--
-- Name: productos_lista_precios_lista_precio_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.productos_lista_precios_lista_precio_seq', 24, true);


--
-- Name: productos_producto_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('aplicacion.productos_producto_seq', 6, true);


--
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles (rol, nombre_rol) FROM stdin;
9	SysAdmin
13	Administrativo
14	roldemo
\.


--
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_rol_seq', 14, true);


--
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.roles_x_selectores (id, rol, selector) FROM stdin;
3	9	36
4	9	2
5	9	3
2	9	11
35	9	57
112	9	96
113	9	97
114	13	57
115	13	96
116	13	97
117	13	36
118	9	98
119	13	98
120	9	100
121	13	100
122	9	101
123	13	101
124	14	36
125	14	96
126	14	97
127	14	98
128	14	100
129	14	101
\.


--
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.roles_x_selectores_id_seq', 129, true);


--
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores (id, superior, descripcion, ord, link) FROM stdin;
3	1	Roles	2	/sistema/rol/
11	1	Acceso menu	3	/sistema/selector/
36	1	Salir	7	/
1	0	Sistema	10	
57	1	Cambio de password	4	/aplicacion/cambiopass/
2	1	Usuarios	1	/sistema/usuario/
95	0	Aplicacion	20	
96	95	Productos	10	/aplicacion/producto/
97	95	Categoria de productos	20	/aplicacion/productocategoria/
99	0	Comercial	30	
101	99	Ventas	20	/comercial/facturaventa/
100	99	Clientes	10	/comercial/cliente/
98	95	Categorias de precios	40	/comercial/preciocategoria/
\.


--
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_id_seq', 101, true);


--
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.selectores_x_webservice_id_seq', 1, false);


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
6	odalis	202cb962ac59075b964b07152d234b70	1606596415759
7	arnaldo	250cf8b51c773f3f8dc8b4be867a9a02	1606149423004
1	root	63a9f0ea7bb98050796b649e85481845	1613106321240
8	demo	fe01ce2a7fbac8fafaed7c982a04e229	1613954027397
\.


--
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_usuario_seq', 8, true);


--
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.usuarios_x_roles (id, usuario, rol) FROM stdin;
197	1	9
207	6	13
208	7	13
209	8	14
\.


--
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.usuarios_x_roles_id_seq', 209, true);


--
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY sistema.webservice (wservice, path, nombre) FROM stdin;
\.


--
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('sistema.webservice_wservice_seq', 1, false);


--
-- Name: apertura_diaria_detalle_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria_detalle
    ADD CONSTRAINT apertura_diaria_detalle_pkey PRIMARY KEY (id);


--
-- Name: apertura_diaria_fecha_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria
    ADD CONSTRAINT apertura_diaria_fecha_key UNIQUE (fecha);


--
-- Name: apertura_diaria_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria
    ADD CONSTRAINT apertura_diaria_pkey PRIMARY KEY (apertura);


--
-- Name: clientes_iden_doc_iden_numero_iden_digito_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes
    ADD CONSTRAINT clientes_iden_doc_iden_numero_iden_digito_key UNIQUE (iden_doc, iden_numero, iden_digito);


--
-- Name: clientes_pkey1; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.clientes
    ADD CONSTRAINT clientes_pkey1 PRIMARY KEY (cliente);


--
-- Name: documento_identificador_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.identificador_documentos
    ADD CONSTRAINT documento_identificador_pkey PRIMARY KEY (iden_doc);


--
-- Name: factura_ventas_detalle_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.factura_ventas_detalle
    ADD CONSTRAINT factura_ventas_detalle_pkey PRIMARY KEY (venta_detalle);


--
-- Name: factura_ventas_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.factura_ventas
    ADD CONSTRAINT factura_ventas_pkey PRIMARY KEY (factura);


--
-- Name: formas_pagos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.formas_pagos
    ADD CONSTRAINT formas_pagos_pkey PRIMARY KEY (forma_pago);


--
-- Name: lista_precios_productos_producto_precio_categoria_key; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.lista_precios_productos
    ADD CONSTRAINT lista_precios_productos_producto_precio_categoria_key UNIQUE (producto, precio_categoria);


--
-- Name: porcentajes_iva_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.porcentajes_iva
    ADD CONSTRAINT porcentajes_iva_pkey PRIMARY KEY (porcentaje_iva);


--
-- Name: precios_categorias_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: hugo
--

ALTER TABLE ONLY aplicacion.precios_categorias
    ADD CONSTRAINT precios_categorias_pkey PRIMARY KEY (precio_categoria);


--
-- Name: productos_categorias_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.productos_categorias
    ADD CONSTRAINT productos_categorias_pkey PRIMARY KEY (categoria);


--
-- Name: productos_lista_precios_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.lista_precios_productos
    ADD CONSTRAINT productos_lista_precios_pkey PRIMARY KEY (lista_precio);


--
-- Name: productos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (producto);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- Name: apertura_diaria_detalle_apertura_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria_detalle
    ADD CONSTRAINT apertura_diaria_detalle_apertura_fkey FOREIGN KEY (apertura) REFERENCES aplicacion.apertura_diaria(apertura) ON DELETE CASCADE;


--
-- Name: apertura_diaria_detalle_producto_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.apertura_diaria_detalle
    ADD CONSTRAINT apertura_diaria_detalle_producto_fkey FOREIGN KEY (producto) REFERENCES aplicacion.productos(producto);


--
-- Name: factura_ventas_detalle_factura_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.factura_ventas_detalle
    ADD CONSTRAINT factura_ventas_detalle_factura_fkey FOREIGN KEY (factura) REFERENCES aplicacion.factura_ventas(factura) ON DELETE CASCADE;


--
-- Name: lista_precios_productos_precio_categoria_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.lista_precios_productos
    ADD CONSTRAINT lista_precios_productos_precio_categoria_fkey FOREIGN KEY (precio_categoria) REFERENCES aplicacion.precios_categorias(precio_categoria);


--
-- Name: lista_precios_productos_producto_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.lista_precios_productos
    ADD CONSTRAINT lista_precios_productos_producto_fkey FOREIGN KEY (producto) REFERENCES aplicacion.productos(producto);


--
-- Name: productos_categoria_fkey; Type: FK CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY aplicacion.productos
    ADD CONSTRAINT productos_categoria_fkey FOREIGN KEY (categoria) REFERENCES aplicacion.productos_categorias(categoria);


--
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES sistema.selectores(id);


--
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES sistema.roles(rol);


--
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY sistema.usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES sistema.usuarios(usuario);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

