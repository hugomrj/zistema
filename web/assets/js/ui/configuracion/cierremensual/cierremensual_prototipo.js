

function CierreMensual(){
    
   this.tipo = "cierremensual";   
   this.recurso = "cierremensual";   
   this.value = 0;
   
   this.dom="";
   this.carpeta=  "/configuracion";   
      
   
   
   this.titulosin = "Cierre Mensual"
   this.tituloplu = "Cierre Mensual"   
      
   this.campoid=  'id';
   this.tablacampos =  ['fecha',  'mes',  'agno'  ];
   this.etiquetas =  ['Fecha',  'Mes', 'Año'];
   
   this.tablaformat =  ['D',  'N', 'N' ];      
   
   this.tbody_id = "cierremensual-tb";
      
   this.botones_lista = [ this.new ] ;
   this.botones_form = "cierremensual-acciones";   
   
   this.parent = null;
   
   /*
    this.combobox = {"tipo_descuento":{
         "value":"tipo_descuento",
         "inner":"descripcion"}};      
   */
   
}







CierreMensual.prototype.new = function( obj  ) {   
    

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
      
      
    document.getElementById( 'cierremensual_agno' ).value = new Date().getFullYear();    
    document.getElementById( 'cierremensual_sel_mes' ).value = (new Date().getMonth() +1);      
    
    
    // botones
    ServicioFijoDescuento_form_add(obj);

    
    
      
};





    

CierreMensual.prototype.form_ini = function() {    
    
    
    
};
 



CierreMensual.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);       




};





CierreMensual.prototype.form_validar = function() {    
    
    
    
    var cierremensual_agno = document.getElementById('cierremensual_agno');     
    if (parseInt(NumQP(cierremensual_agno.value)) <= 0 )         
    {
        
        msg.error.mostrar("Falta Año");            
        cierremensual_agno.focus();
        cierremensual_agno.select();         
        
        return false;
    }        
    
    
    
    return true;
    
};






CierreMensual.prototype.carga_combos = function( obj  ) {                
            

};





CierreMensual.prototype.post_form_id = function( obj  ) {                
    
        ServicioFijoDescuento_form_reg(  obj  );
   
};








