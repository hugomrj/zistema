

function ConceptoServicio(){
    
   this.tipo = "conceptoservicio";   
   this.recurso = "conceptoservicio";   
   this.value = 0;
   this.form_descrip = "descripcion";
   this.json_descrip = "descripcion";
   
   this.dom="";
   this.carpeta=  "/configuracion";   
      
      
   this.titulosin = "Concepto servicio"
   this.tituloplu = "Conceptos servicios"   
      
   
   this.campoid=  'servicio';
   this.tablacampos =  ['servicio', 'descripcion', 'tipo_descuento.descripcion',  'monto_fijo'  ];
   
    this.etiquetas =  ['Servicio', 'Descripcion', 'Tipo descuento', 'Monto fijo' ];                                  
    
    this.tablaformat = ['N', 'C', 'C', 'N' ];                                  
   
   this.tbody_id = "conceptoservicio-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "conceptoservicio-acciones";   
      
   
   this.parent = null;
   
   
    this.combobox = {"tipo_descuento":{
         "value":"tipo_descuento",
         "inner":"descripcion"}};   
   
}





ConceptoServicio.prototype.lista_new = function( obj  ) {                

    reflex.form_new( obj ); 
    reflex.acciones.button_add(obj);        
    
};



ConceptoServicio.prototype.form_ini = function() {    
    
    var conceptoservicio_monto_fijo = document.getElementById('conceptoservicio_monto_fijo');          
    conceptoservicio_monto_fijo.onblur  = function() {                
        conceptoservicio_monto_fijo.value  = fmtNum(conceptoservicio_monto_fijo.value);
    };     
    conceptoservicio_monto_fijo.onblur();          

    

    
    var conceptoservicio_cantidad = document.getElementById('conceptoservicio_cantidad');          
    conceptoservicio_cantidad.onblur  = function() {                
        conceptoservicio_cantidad.value  = fmtNum(conceptoservicio_cantidad.value);
    };     
    conceptoservicio_cantidad.onblur();          

    
    var conceptoservicio_actual = document.getElementById('conceptoservicio_actual');          
    conceptoservicio_actual.onblur  = function() {                
        conceptoservicio_actual.value  = fmtNum(conceptoservicio_actual.value);
    };     
    conceptoservicio_actual.onblur();          
    
    

    
    
    
    
    
    
};



ConceptoServicio.prototype.form_validar = function() {    
    return true;
};







ConceptoServicio.prototype.carga_combos = function( obj  ) {                
    
   
   
        var tipo_descuento_tipo_descuento = document.getElementById("tipo_descuento_tipo_descuento");
        var idedovalue = tipo_descuento_tipo_descuento.value;
   
        ajax.url = html.url.absolute()+'/api/tiposdecuentos/all' ;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();               
                
        var oJson = JSON.parse( datajson ) ;
        
        for( x=0; x < oJson.length; x++ ) {
            
            var jsonvalue = (oJson[x]['tipo_descuento'] );            
            
            if (idedovalue != jsonvalue )
            {  
                var opt = document.createElement('option');            
                opt.value = jsonvalue;
                opt.innerHTML = oJson[x]['descripcion'];                        
                tipo_descuento_tipo_descuento.appendChild(opt);                     
            }
            
        }
            

};







